## Template Engine Kata



### Requirement

*Taken from **Test Driven, Lasse Koskela***

Provide a template engine that can read a template with placeholders marked up, which can be replaced
with values.


- The system replaces variable placeholders like ${firstname} and ${lastname}
from a template with values provided at runtime.
- The system attempts to send a template with some variables not populated
will raise an error.
- The system silently ignores values for variables that aren’t found from the
template.
- The system supports the full Latin-1 character set in templates.
- The system supports the full Latin-1 character set in variable values.

### Tests

Evaluating template “Hello, ${name}” with the value “Reader” for variable
“name” results in the string “Hello, Reader”.

#### Test 1
Write a failing test

```
package eg.kata;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TemplateTest {

    @Test
    void oneVariable() {
        assertEquals("Hello, Reader",
          Template.of("Hello, ${name}")
          .set("name", "Reader")
          .evaluate());
    }
}

```

Compiling failing version:
```
package eg.kata;

public class Template {


    public static Template of(String template) {
        return new Template();
    }

    public Template set(String name, String value) {
        return this;
    }

    public static String evaluate() {
        return null;
    }
}

```
```
org.opentest4j.AssertionFailedError:
Expected :Hello, Reader
Actual   :null
 <Click to see difference>


	at org.junit.jupiter.api.AssertionUtils.fail(AssertionUtils.java:54)
	at org.junit.jupiter.api.AssertEquals.failNotEqual(AssertEquals.java:195)
	at org.junit.jupiter.api.AssertEquals.assertEquals(AssertEquals.java:184)
	at org.junit.jupiter.api.AssertEquals.assertEquals(AssertEquals.java:179)
	at org.junit.jupiter.api.Assertions.assertEquals(Assertions.java:508)
```

Minimum needed to pass:

```
package eg.kata;

public class Template {


    public static Template of(String template) {
        return new Template();
    }

    public Template set(String name, String value) {
        return this;
    }

    public String evaluate() {
        return "Hello, Reader";
    }
}

```

Of course the code is only valid for this specific tests.

#### Test 2
**We use further specific tests to make the code more general.**

*Triangulate with a different value to discover coding requirements*

```
    @Test
    void oneVariableSomeOneElse() {
        assertEquals("Hello, Someone",
                Template.of("Hello, ${name}")
                        .set("name", "Someone")
                        .evaluate()
        );
    }
```

Again the minimum code written to pass the test:

```
package eg.kata;

public class Template {

    String value;


    public static Template of(String template) {
        return new Template();
    }

    public Template set(String name, String value) {
        this.value = value;
        return this;
    }

    public String evaluate() {
        return "Hello, " + this.value;
    }
}
```

#### Test 3

```
    @Test
    void multiVariable() {
        assertEquals("1, 2, 3",
                Template.of("${one}, ${two}, ${three}")
                        .set("one", "1")
                        .set("two", "2")
                        .set("three", "3")
                        .evaluate()
        );
    }
```

Highlights the need to refactor:

```
    @Test
    void multiVariable() {
        assertEquals("1, 2, 3",
                Template3.of()
                        .template("${one}, ${two}, ${three}")
                        .set("one", "1")
                        .set("two", "2")
                        .set("three", "3")
                        .evaluate()
        );
    }
```

Code changes to pass the above

```
   private Map<String, String> variables = new HashMap<>();
    private String template;


    public static Template3 of() {
        return new Template3();
    }

    public Template3 template(String template) {
        this.template = template;
        return this;
    }

    public Template3 set(String name, String value) {
        this.variables.put(name, value);
        return this;
    }

    public String evaluate() {

        String[] keys = variables
                .keySet()
                .toArray(new String[0]);

        String r = template;
        for (String k : keys) {
            r = r.replaceAll("\\$\\{" + k + "\\}", variables.get(k));
        }

        return r;
    }
```

### Test 4

```
    @Test
    void missVariableValue() {
        assertThrows(Exception.class,
                () ->
                Template4.of()
                        .template("${one}, ${two}, ${three}")
                        .set("one","1")
                        .evaluate());
    }
```

Code
```
  public String evaluate() {

        String[] keys = variables
                .keySet()
                .toArray(new String[0]);

        String r = template;
        for (String k : keys) {
            r = r.replaceAll("\\$\\{" + k + "\\}", variables.get(k));
        }

        if (r.matches(".*\\$\\{.+\\}.*"))
            throw new RuntimeException("Missing value");
        return r;
    }

```

Refactored (using Command/Control+Shift M in IntelliJ)
```
    public String evaluate() {

        String[] keys = variables
                .keySet()
                .toArray(new String[0]);

        String r = template;
        r = replaceVariables(keys, r);

        checkForMissingValues(r);
        return r;
    }

    private void checkForMissingValues(String r) {
        if (r.matches(".*\\$\\{.+\\}.*"))
            throw new RuntimeException("Missing value");
    }

    private String replaceVariables(String[] keys, String r) {
        for (String k : keys) {
            r = r.replaceAll("\\$\\{" + k + "\\}", variables.get(k));
        }
        return r;
    }
```

### Test 5
Check for missing value in message

```
    @Test
    void missVariableValue() {
        Throwable throwable = assertThrows(Exception.class,
                () ->
                Template4.of()
                        .template("${one}, ${two}, ${three}")
                        .set("one","1")
                        .evaluate());
        assertEquals("Missing value ${two}, ${three}", throwable.getMessage());
    }

```

Code


```
    public String evaluate() {

        String[] keys = variables
                .keySet()
                .toArray(new String[0]);

        String r = template;
        r = replaceVariables(keys, r);

        checkForMissingValues(r);
        return r;
    }

    private void checkForMissingValues(String r) {
        Matcher m = Pattern.compile("\\$\\{.+\\}").matcher(r);
        if (m.find())
            throw new RuntimeException("Missing value " + m.group());
    }

    private String replaceVariables(String[] keys, String r) {
        for (String k : keys) {
            r = r.replaceAll("\\$\\{" + k + "\\}", variables.get(k));
        }
        return r;
    }

```

### Test 6


Verify performance requirements

Performance:
- 100 word template,
- 20 variables,
- values of 15 characters
- Evaluated in < 200 ms

```
    @Test
    void performanceOf100words20Vars15charValuesa() {

        String t = templateOf100();

        Long now = Instant.now().toEpochMilli();
        String r = set15charValues(Template4.of()
                .template(t))
                .evaluate();
        Long timeTaken = Instant.now().toEpochMilli() - now;
        assertTrue(timeTaken < 200L);
    }

    private Template4 set15charValues(Template4 template) {
        for(int i=1; i < 101; i++)
        {
            template = template.set(i+"", String.format("%15s", i));
        }
        return template;
    }



    private String templateOf100() {
        StringBuilder sb = new StringBuilder();
        for(int i=1; i < 101; i++) {
            sb.append("${").append(i).append("} ");
        }
        return sb.toString();
    }
```

Refactor code, fluent centric

```
   public String evaluate() {
        return replaceVariables()
                .checkForMissingValues();
    }

    private String checkForMissingValues() {
        Matcher m = Pattern.compile("\\$\\{.+\\}").matcher(template);
        if (m.find())
            throw new RuntimeException("Missing value " + m.group());

        return template;
    }

    private Template4 replaceVariables() {
        String[] keys = variables
                .keySet()
                .toArray(new String[0]);
        for (String k : keys) {
            template = template.replaceAll("\\$\\{" + k + "\\}", variables.get(k));
        }
        return this;
    }
```


### Test 7

Template value is itself a variable

```
  String result = Template.of().template("${var}")
   .set("var", "${newvar}")
   .evaluate();
  assertEquals("${newvar}", result)

```

### Test 8

Refactor the code to clean code principles, small (~15 lines) methods
and readable code.

Running the tests should prove nothing is broken.


### Test 9

Do variables get processed only once?

The following will cause:

`java.lang.IllegalArgumentException: No group with name {one}`


```
    @Test
    void variablesProcessedJustOnce() {
        String result = Template4.of()
                .template("${one}, ${two}, ${three}")
                .set("one","${one}")
                .set("two","${three}")
                .set("three","${two}")
                .evaluate();
        assertEquals("${one} ${three}, ${two}", result);
    }
```


This indicates a new for redesign of the parsing algorithm.

Add a new TemplateParser class to
- split a template to text and variables segments
- do it using test first development
- once the parser is done use it within Template


### Test 10

Initial parse test would look something like this:

```
public class TestTemplateParse {
              @Test
              public void emptyTemplateRendersAsEmptyString() throws Exception {
                  TemplateParse parse = new TemplateParse();
                  List<String> segments = parse.parse("");
                  assertEquals("Number of segments", 1, segments.size());
                  assertEquals("", segments.get(0));
} }
```

The implementation makes use of Regex features eg.

```
    Pattern pattern = Pattern.compile("\\$\\{[^}]*\\}");
    Matcher matcher = pattern.matcher(src);
    int index = 0;
    while (matcher.find()) {
        addPrecedingPlainText(segs, src, matcher, index);
        addVariable(segs, src, matcher);
        index = matcher.end();
      }
```

For further help see chap 3 of teh TDD book by Lasse Koskela.
