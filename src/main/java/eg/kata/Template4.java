package eg.kata;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Template4 {

    private Map<String, String> variables = new HashMap<>();
    private String template;


    public static Template4 of() {
        return new Template4();
    }

    public Template4 template(String template) {
        this.template = template;
        return this;
    }

    public Template4 set(String name, String value) {
        this.variables.put(name, value);
        return this;
    }

    public String evaluate() {
        return replaceVariables()
                .checkForMissingValues();
    }

    private String checkForMissingValues() {
        Matcher m = Pattern.compile("\\$\\{.+\\}").matcher(template);
        if (m.find())
            throw new RuntimeException("Missing value " + m.group());

        return template;
    }

    private Template4 replaceVariables() {
        String[] keys = variables
                .keySet()
                .toArray(new String[0]);
        for (String k : keys) {
            template = template.replaceAll("\\$\\{" + k + "\\}", variables.get(k));
        }
        return this;
    }


}
