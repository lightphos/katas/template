package eg.kata;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Template3 {

    private Map<String, String> variables = new HashMap<>();
    private String template;


    public static Template3 of() {
        return new Template3();
    }

    public Template3 template(String template) {
        this.template = template;
        return this;
    }

    public Template3 set(String name, String value) {
        this.variables.put(name, value);
        return this;
    }

    public String evaluate() {

        String[] keys = variables
                .keySet()
                .toArray(new String[0]);

        String r = template;
        for (String k : keys) {
            r = r.replaceAll("\\$\\{" + k + "\\}", variables.get(k));
        }

        return r;
    }


}
