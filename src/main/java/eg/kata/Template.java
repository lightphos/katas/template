package eg.kata;

public class Template {

    String value;


    public static Template of(String template) {
        return new Template();
    }

    public Template set(String name, String value) {
        this.value = value;
        return this;
    }

    public String evaluate() {
        return "Hello, " + this.value;
    }
}
