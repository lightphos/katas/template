package eg.kata;

import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TemplateTest {

    @Test
    void oneVariable() {
        assertEquals("Hello, Reader",
          Template.of("Hello, ${name}")
                  .set("name", "Reader")
                  .evaluate()
        );
    }

    @Test
    void oneVariableSomeOneElse() {
        assertEquals("Hello, Someone else",
                Template.of("Hello, ${name}")
                        .set("name", "Someone else")
                        .evaluate()
        );
    }


    @Test
    void multiVariable() {
        assertEquals("1, 2, 3",
                Template3.of()
                        .template("${one}, ${two}, ${three}")
                        .set("one", "1")
                        .set("two", "2")
                        .set("three", "3")
                        .evaluate()
        );
    }

    @Test
    void multiVarMissingTemplateVar() {
        assertEquals("1, 2, 3",
                Template3.of()
                        .template("${one}, ${two}, ${three}")
                        .set("one", "1")
                        .set("two", "2")
                        .set("three", "3")
                        .set("four", "4")
                        .evaluate()
        );

    }

    @Test
    void missVariableValue() {
        Throwable throwable = assertThrows(Exception.class,
                () ->
                Template4.of()
                        .template("${one}, ${two}, ${three}")
                        .set("one","1")
                        .evaluate());
        assertEquals("Missing value ${two}, ${three}", throwable.getMessage());
    }

    @Test
    void performanceOf100words20Vars15charValuesa() {

        String t = templateOf100();

        Long now = Instant.now().toEpochMilli();
        String r = set15charValues(Template4.of()
                .template(t))
                .evaluate();
        Long timeTaken = Instant.now().toEpochMilli() - now;
        System.out.println(timeTaken);
        assertTrue(timeTaken < 200L,"Time taken " + timeTaken);
    }

    @Test
    void variablesProcessedJustOnce() {
        String result = Template4.of()
                .template("${one}, ${two}, ${three}")
                .set("one","${one}")
                .set("two","${three}")
                .set("three","${two}")
                .evaluate();
        assertEquals("${one} ${three}, ${two}", result);
    }

    private Template4 set15charValues(Template4 template) {
        for(int i=1; i < 101; i++)
        {
            template = template.set(i+"", String.format("%15s", i));
        }
        return template;
    }



    private String templateOf100() {
        StringBuilder sb = new StringBuilder();
        for(int i=1; i < 101; i++) {
            sb.append("${").append(i).append("} ");
        }
        return sb.toString();
    }


}
